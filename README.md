# Diploma



## Terraform

Terraform code to create Kubernetes cluster in AWS (EKS)

## Deploy EKS

- You need to have an account in AWS, installed aws cli.
- Edit terraform/variables.tf file with your AWS region and cluster name
- Configure your AWS environment with the following command:

```
aws configure
```
- Execute commands:

```
terraform init
terraform apply
```

## Helm

Helm chart is used to deploy web-application in https://gitlab.com/ykuleshova/app.git
