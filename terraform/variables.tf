variable "region" {
  default = "eu-central-1"
  description = "AWS region"
}

variable "cluster_name" {
  default = "diploma-eks"
}

locals {
  cluster_enabled_log_types = ["api", "audit"]
}